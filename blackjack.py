import random

class Player():
  Hand = 0
  HandDisplay = ''
  
  def updateHandValue(self, card):
    if card>10:
      card = 10
    self.Hand = self.Hand+card
    
  def updateHandValueDisplay(self, card): 
    if card == 1:
      card = 'A'
    elif card == 11:
      card = 'J'
    elif card == 12:
      card = 'Q'
    elif card == 13:
      card = 'K'
    self.HandDisplay = self.HandDisplay + ' ' + str(card)
    
  def dealCard(self):
    # return random.randint(1,13)
    card = random.randint(1,13)
    self.updateHandValue(card)
    self.updateHandValueDisplay(card)
  
  def selfReset(self):
    self.Hand = 0
    self.HandDisplay = ''
    
  def getHandValue(self):
    # This function treats A as 1 or 11
    if 'A' in self.HandDisplay and self.Hand<=11:
      return self.Hand+10 # Ace is added as 1 so the bonus 10 makes it treated as 11
    else:
      return self.Hand

# End of Player Class
def printHeader(coin, win, lose):
  print('**************************')
  print('* BlackJack')
  print('* Your Coins: ',coin,)
  print('* Your Wins: ',win,)
  print('* Your losses: ',lose,)
  print('**************************')

# ======================================================
# Welcome to the Game
coins = 500
wins = 0
loss = 0
gambler = Player()
dealer = Player()
playerGetsTurn = True
dealerGetsTurn = True

printHeader(coins,wins,loss)
gamerunning = True

while gamerunning:
  try:
    # Betting Phase
    bet = int(input('Start a game by betting any amount of coins: '))
    # ====================================================
    # Coins
    if bet > coins:
      print('You dont have that much amount of coins')
      continue
    else:
      coins = coins - bet
    # ====================================================
      dealer.dealCard()
    # Start Player Hand
    for x in range (0,2):
      gambler.dealCard()
    print("Dealer's Hand:", dealer.HandDisplay)
    # gambler.HandDisplay = " A 10"
    print('Your Hand:', gambler.HandDisplay)
    # Check for Blackjack
    if (gambler.getHandValue()==21):
          print ('You Got Blackjack!')
          wins = wins + 1
          coins = coins + (bet*2)
          playerGetsTurn = False
          dealerGetsTurn = False
    # ====================================================
    # Hit or Stand
    choice = ''
    while playerGetsTurn:
      choice = input('Type h to Hit, type s to Stand: ')
      if choice == 'h': # Hit
        gambler.dealCard()
        print('Your Hand:', gambler.HandDisplay)
        if (gambler.getHandValue()>21):
          print ('You Lose')
          loss = loss + 1
          dealerGetsTurn = False
          break
        continue
      if choice == 's': # Stand
        break
    # ====================================================
    # Dealers Turn Starts Here
    if dealerGetsTurn == True:
      while dealer.getHandValue() < gambler.getHandValue() and dealer.getHandValue() != 21:
        dealer.dealCard()
      print ("Dealer's Hand:", dealer.HandDisplay)
      if dealer.getHandValue()>21:
        print('You Win')
        coins = coins + (bet*2)
        wins = wins + 1
      elif dealer.getHandValue()>gambler.getHandValue():
        print('You Lose')
        loss = loss + 1
      elif dealer.getHandValue() == gambler.getHandValue():
        print('Draw')
        coins = coins + bet
    
    #Cleanup
    gambler.selfReset()
    dealer.selfReset()
    printHeader(coins,wins,loss)
    playerGetsTurn = True
    dealerGetsTurn = True
  except (ValueError):
    print('Please Follow the Instructions')
    continue

