# Game-App

#### Version 0.00
    * Repository Created

#### Version 1.00
    * Basic Blackjack Mechanics Implemented
    * Playable State (Terminal Application). Hopefully Bug Free

#### Version 1.12
    * Dealers hand now visible
    * Special Rule "Natural" Implemented
    * Ace card is now treated as 1 or 11 (Rule of "Soft" Hand)
    
#### Version 1.15
    * Dealer can "Natural" now.

#### Version 1.15.1
    * Now we're using proper Blackjack Terminology